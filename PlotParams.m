% 入口参数
% Data：数据矩阵，来源于CPUAnaModel.m
% ParamMatrix：参数矩阵，来源于CPUANaModel.m中的paramMatC
% Selecteds：所需要画图的对象
% Pid：暂时未用，以后可能用于挑选特定Pid的进行绘图
% Usage：在CPUAnaModel中可直接调用 PlotParams(sortedData, paramMatC, {'RenameROBFull'; 'IQFull'; 'LSQFull'}
%

% 2015-01-12 v1.0: happyhls@gmail.com 初始版本
% 2015-01-13 v1.1: happyhls@gmail.com 增加对多个参数的绘图

function PlotParams(Data, ParamMatrix, Selecteds, Pid)
ParamMatrix = ParamMatrix( : , 1);
tick = Data(:, end);
[imagesCount, ~] = size(Selecteds);

colors = ['r','g','b','w','k', 'm', 'c', 'y'];
for i = 1 : imagesCount
	selectedImage = Selecteds{i, 1}
	[paramsCount, ~] = size(selectedImage);
	figure
	selectedData = {};
    titleMessage = '';
	for j = 1 : paramsCount
		selectedItem = selectedImage{j, 1};
		[rowI, ~] = find(ismember(ParamMatrix, selectedItem));
		selectedData{j, 1} = selectedItem;
		selectedData{j, 2} = Data(:, rowI);
		selectedData{j, 3} = mean(Data(:, rowI));
	end
	sortedData = sortrows(selectedData, [-3])
    for j = 1 : paramsCount
		color = colors(j);
		hold on
		result = [tick, [sortedData{j, 2}]];
		result = sortrows(result, [1]);
		plot(result(:,1), result(:,2), color);
		titleMessage = strcat(titleMessage, sortedData{j, 1} ,' @ ', color , '  ||  ');
	end
    title(titleMessage);
    xlabel('tick');
end
